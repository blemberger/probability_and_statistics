# Measured bond strengths for concrete/composite
bond_strength = read.csv(file="ex1.11.csv", head=TRUE)

# What columns are in the data set?
names(bond_strength)

# Show the first 10 entries
head(bond_strength, 10)

# Count number of observations in the sample
n = length(bond_strength$measured_bond_strength)
n

# Give descriptive statistics
summary(bond_strength)

# Calculate relative frequencies (hist() does not provide an option for this :-( )
#rel_freq = bond_strength$measured_bond_strength/sum(bond_strength$measured_bond_strength)

# Show relative-frequence histogram of concrete strength data with equal sized classes
png("hist_equal_classes_ex_1.11.png", width=700, height=480)
# Create a vector to hold tick-marks and class size to be 0-28 width of 2
ticks = seq(0,28, by=2)
hist(bond_strength$measured_bond_strength,
    main="Measured bond strengths for concrete & composite", xlab="Measured Bond Strength",
    breaks=ticks,
    right=FALSE,
    col="lightblue",
    freq= FALSE, # use relative frequencies, not frequency counts
    axes= FALSE) # custom axes set up in next two statements
axis(1, ticks) # bottom axis, ticks as specified in vector
axis(2) # left axis takes computed defaults

dev.off()

# Show relative frequency histogram of concrete strenghth using variable sized classes
png("hist_var_classes_ex_1.11.png", width=700, height=480)
# Create a vector to hold tick-marks and class size to be 0-28 width of 2
ticks = c(2, 4, 6, 8, 12, 20, 30, 40)
hist(bond_strength$measured_bond_strength,
    main="Measured bond strengths for concrete & composite", xlab="Measured Bond Strength",
    breaks=ticks,
    right=FALSE,
    col="lightblue",
    axes= FALSE) # custom axes set up in next two statements
axis(1, ticks) # bottom axis, ticks as specified in vector
axis(2) # left axis takes computed defaults

dev.off()