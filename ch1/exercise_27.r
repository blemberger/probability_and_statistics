# Exercise 1.27
# Drill lifetimes (number of bored holes until failure)

# Read tab separated data in. Default separator for read.csv is ','
drill_lifetimes = read.csv(file="~/training/R/PandS4EandS/ch1/exercise_27.csv", sep="")

# Utility function to convert a list of density values to a string of relative percentages as strings
convertToFormattedRelPercent = function(densities, bin_width, format_str) {
  # Convert each density value to a relative frequency % by multiplying by 100 * bin size
  rel_percent = densities * bin_width * 100
  # Format the frequency values in 00.0% format:
  rel_freq_labels = lapply(rel_percent, function(e) {sprintf(format_str, e)})
  rel_freq_labels
}

# Part b) Show frequendy histogram of drill lifetime (linear)
png("hist_exercise27_linear.png", width=700, height=480)

# Create a vector to hold tick-marks and class size to be 0-550 width of 50
lin_width = 50
ticks = seq(0,550, by=lin_width)
h_lin = hist(drill_lifetimes$drill_lifetime,
     main="Life Distribution of Microdrills",
     xlab="Drill Lifetime (# holes drilled)",
     breaks=ticks,
     right=FALSE,
     col="steelblue2",
     freq=FALSE,
     ylim=c(0, 0.01), # hint that top tick on y-axis for density should be 0.01
     ylab=NULL,
     axes= FALSE) # custom axes set up in next two statements

axis(1, ticks) # bottom axis, ticks as specified in vector
old_ticks = axTicks(2) # obtain the tick marks that hist would have calculated
# create the y axis by changing the labels to relative frequencies rather than densities
axis(2, at=old_ticks, labels=convertToFormattedRelPercent(old_ticks, lin_width, "%.0f"))
title(ylab="Relative Frequency %")

# To print labels at the top of each bar with the rel. freq. value:
text(x=h_lin$mids, y=h_lin$density, labels = convertToFormattedRelPercent(h_lin$density, lin_width, "%2.1f%%"), pos = 3)

dev.off()

# Part c) Do histogram for ln(drill lifetime)
png("hist_exercise27_log.png", width=700, height=480)

# Create a vector to hold tick-marks and class size to be 0-7 width of 0.5
log_width = 0.5
ticks = seq(2.25,6.25, by=log_width) # use same log ranges as book chose for ex. answer
drill_lifetimes$ln_lifetimes = log(drill_lifetimes$drill_lifetime)
h_log = hist(drill_lifetimes$ln_lifetimes,
         main="Life Distribution of Microdrills",
         xlab="log(# holes drilled)",
         breaks=ticks,
         right=FALSE,
         col="steelblue2",
         freq=FALSE,
         ylim=c(0, 0.8), # hint that top tick on y-axis for density should be 40% / 0.5
         ylab=NULL,
         axes= FALSE)

axis(1, ticks) 
old_ticks = axTicks(2) 
axis(2, at=old_ticks, labels=convertToFormattedRelPercent(old_ticks, log_width, "%.0f"))
title(ylab="Relative Frequency %")
text(x=h_log$mids, y=h_log$density, labels = convertToFormattedRelPercent(h_log$density, log_width, "%2.1f%%"), pos = 3)
dev.off()

# Part d) What proportion of the lifetime observations are < 100?
prop = sum(h_lin$density[h_lin$breaks < 100]) * lin_width # density * class width
print(sprintf("Proportion of lifetime observations < 100: %.2f", prop))

# What proportion of the lifetime observations are >= 200?
prop = 1 - sum(h_lin$density[h_lin$breaks < 200]) * lin_width # 1 - (prop < 200)
print(sprintf("Proportion of lifetime observations >= 200: %.2f", prop))
