# Exercise 1.17
# Collusion of bidding within the construction industry

# Read tab separated data in. Default separator for read.csv is ','
collusion = read.csv(file="~/training/R/PandS4EandS/ch1/exercise_17.csv", sep="")

# Append a new column to the collusion data frame, cum_contracts, which is the cumulative values of num_contracts
collusion$cum_contracts = cumsum(collusion$num_contracts)

# Part a) What proportion of contracts involved at most five bidders?
at_most_five = collusion[collusion$num_bidders == 5, ]$cum_contracts
total_contracts = tail(collusion, 1)$cum_contracts
prop_at_most_five = at_most_five / total_contracts
print(sprintf("%d contracts with at most five bidders / %d total contacts =  %f", at_most_five, total_contracts, prop_at_most_five))

# What proportion of contracts involved at least five bidders (5 or more bidders)?
at_least_five = total_contracts - collusion[collusion$num_bidders == 4, ]$cum_contracts
prop_at_least_five = at_least_five / total_contracts
print(sprintf("%d contracts with at least five bidders / %d total contacts =  %f", at_least_five, total_contracts, prop_at_least_five))

# Part b) What proportion of contracts involved 5 - 10 bidders inclusive?
at_most_ten = collusion[collusion$num_bidders == 10, ]$cum_contracts
less_than_five = collusion[collusion$num_bidders == 4, ]$cum_contracts
five_to_ten = at_most_ten - less_than_five
prop_five_ten = five_to_ten / total_contracts
print(sprintf("%d contracts with between five and ten bidders inclusive / %d total contacts =  %f", five_to_ten, total_contracts, prop_five_ten))

# What proportion of contracts involved 5 - 10 bidders non-inclusive?
less_than_ten = collusion[collusion$num_bidders == 9, ]$cum_contracts
five_or_less = collusion[collusion$num_bidders == 5, ]$cum_contracts
five_to_ten = less_than_ten - five_or_less
prop_five_ten = five_to_ten / total_contracts
print(sprintf("%d contracts with between five and ten bidders non-inclusive / %d total contacts =  %f", five_to_ten, total_contracts, prop_five_ten))

# Show frequendy histogram of number of contracts with equal sized classes bid number
png("hist_exercise17.png", width=700, height=480)

# This is a barplot, not a histogram technically. R's hist() function wants
#   to do a frequency calculation for a single column of data and create bars
#   each with height corresponding to the frequence and with width (bins) of the column's possible values (classes).

# barplot() does not calculate relative frequency by itself, so we do it:
rel_freq_contracts = collusion$num_contracts / total_contracts
contract_plot = barplot(rel_freq_contracts,
     ylim=c(0, 0.27),
     main="Detection of Collusive Behavior in the Construction Industry",
     xlab="Number of Bidders",
     ylab="Relative Number of Contracts",
     names.arg=collusion$num_bidders,
     col="wheat3")

# Format the frequency values in 0.000 format:
rel_freq_labels = round(rel_freq_contracts, digits = 3)

# To print labels at the top of each bar with the rel. freq. value:
text(x=contract_plot, y=rel_freq_contracts, labels = rel_freq_labels, pos = 3)

dev.off()