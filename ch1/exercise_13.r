library(aplpack)  # Gives better stem & leaf support than base R

# Tensile ultimate strength (ksi)
ult_strength = read.csv(file="~/training/R/PandS4EandS/ch1/exercise_13.csv", head=TRUE)

# What columns are in the data set?
names(ult_strength)

# Show the first 10 entries
head(ult_strength, 10)

# Count number of observations in the sample
n = length(ult_strength$ultimate_strength)
n

# Give descriptive statistics
summary(ult_strength)

# Use stem.leaf from aplpack package, as it allows control over the plot
#   unit=1.0 specifies the units of the stem
#   there is no need to do a truncation of the dataset, as setting unit=1.0 automatically
#     truncates the digits in the 0.1 place
#   m=5 species to subdivide the stem units into 5 equal parts
#   style=bare specifies to use the non-Tukey (regular numbers) notation for the stem values
#   depths=FALSE prevents the display of frequencies for each stem
stem.leaf(ult_strength$ultimate_strength, unit=1.0, m=5, style="bare", depths=FALSE)

# Show frequendy histogram of ultimate strength data with equal sized classes
png("hist_exercise13.png", width=700, height=480)

# Create a vector to hold tick-marks and class size to be 122-148 width of 2
ticks = seq(122,148, by=2)
hist(ult_strength$ultimate_strength,
    main="Establishing Mechanical Property Allowables for Metals",
    xlab="Tensile Ultimate Strength (ksi)",
    breaks=ticks,
    right=FALSE,
    col="steelblue2",
    ylim=c(0, 40), # hint that top tick on y-axis should be 40
    axes= FALSE) # custom axes set up in next two statements

axis(1, ticks) # bottom axis, ticks as specified in vector
axis(2) # left axis takes computed defaults

dev.off()