# Exercise 1.19
# contaminating particles on silicon wafers

# Read tab separated data in. Default separator for read.csv is ','
contaminant_particles = read.csv(file="~/training/R/PandS4EandS/ch1/exercise_19.csv", sep="")

# Append a new column to the contaminant_particles data frame, cum_frequency, which is the cumulative frequenciess
contaminant_particles$cum_frequency = cumsum(contaminant_particles$frequency)

total_contracts = tail(contaminant_particles, 1)$cum_frequency
contaminant_particles$rel_frequency = contaminant_particles$frequency / total_contracts
contaminant_particles$cum_rel_frequency = contaminant_particles$cum_frequency / total_contracts

# Part a) What proportion of sampled wafers has at least one particle?
#   100% - (% with 0 particles)
prop = 1.0 - sum(contaminant_particles[contaminant_particles$num_particles == 0, ]$cum_rel_frequency)
print(sprintf("Proportion of sampled wafers with at least one particle: %f", prop))

# Part a) What proportion of sampled wafers has at least five particles?
#   100% - (% with 4 particles)
prop = 1.0 - sum(contaminant_particles[contaminant_particles$num_particles == 4, ]$cum_rel_frequency)
print(sprintf("Proportion of sampled wafers with at least five particles: %f", prop))

# Part b) What proportion of sample wafers had 5 - 10 particles inclusive?
at_most_ten = contaminant_particles[contaminant_particles$num_particles == 10, ]$cum_rel_frequency
less_than_five = contaminant_particles[contaminant_particles$num_particles == 4, ]$cum_rel_frequency
print(sprintf("Proportion of sampled wafers with at 5 - 10 particles inclusive: %f", at_most_ten - less_than_five))

# Part b) What proportion of sample wafers had 5 - 10 particles non-inclusive?
less_than_ten = contaminant_particles[contaminant_particles$num_particles == 9, ]$cum_rel_frequency
at_most_five = contaminant_particles[contaminant_particles$num_particles == 5, ]$cum_rel_frequency
print(sprintf("Proportion of sampled wafers with at 5 - 10 particles non-inclusive: %f", less_than_ten - at_most_five))

# Show frequendy histogram of number of contracts with equal sized classes bid number
png("hist_exercise19.png", width=700, height=480)

# This is a barplot, not a histogram technically. R's hist() function wants
#   to do a frequency calculation for a single column of data and create bars
#   each with height corresponding to the frequence and with width (bins) of the column's possible values (classes).

plot = barplot(contaminant_particles$rel_frequency,
     ylim=c(0, 0.20),
     main="Contaminating Particles on Silicon Wafers",
     xlab="Number of Particles",
     ylab="Frequency",
     names.arg=contaminant_particles$num_particles,
     col="yellow")

# Format the frequency values in 0.00 format:
#rel_freq_labels = signif(contaminant_particles$rel_frequency, digits = 2)
rel_freq_labels = sprintf("%1.2f", contaminant_particles$rel_frequency)

# To print labels at the top of each bar with the rel. freq. value:
text(x=plot, y=contaminant_particles$rel_frequency, labels = rel_freq_labels, pos = 3)

dev.off()